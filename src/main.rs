use rayon::prelude::*;
use indicatif::{ProgressBar, ProgressStyle, ParallelProgressIterator};
use std::{fs::File, iter::Iterator, error::Error, io::prelude::*, env};


const BASE: u64 = 10;

// a=7, b=8, n=903225806451612
const LOWER: u64 = 1;
const UPPER: u64 = 20_000_000;
const FILENAME: &'static str = "out.csv";


fn main() -> Result<(), Box<dyn Error>> {
    let mut args = env::args();
    args.next();
    let filename = args.next().unwrap_or(FILENAME.to_string());
    let lower = args.next().map(|s| s.parse::<u64>()).transpose().unwrap_or(None).unwrap_or(LOWER);
    let upper = args.next().map(|s| s.parse::<u64>()).transpose().unwrap_or(None).unwrap_or(UPPER);

    let progbar = ProgressBar::new(upper - lower)
                    .with_style(
                        ProgressStyle::default_bar()
                            .progress_chars("=>.")
                            .template("{percent:>3.0}% [{bar:60}] eta: {eta_precise}"));

    let mut results: Vec<_> = (lower..upper)
            .into_par_iter()
            .progress_with(progbar.clone())  // ew
            .flat_map_iter(|n| check_n(n))
            .map(|(a, b, n)| {
                let num = a * next_base_pow(n) + n;
                let denom = BASE * n + b;
                let s = if num % a == 0 && denom % b == 0 && num * b == denom * a {
                    ""
                }
                else {
                    "FAILED "
                };
                progbar.println(format!("{}{} / {} = {}{} / {}{}", s, a, b, a, n, n, b));

                (a, b, n)
            })
            .collect();
    results.sort_unstable();

    let mut file = File::create(filename)?;
    file.write_all("a,b,n\n".as_bytes())?;
    for (a, b, n) in results.iter() {
        file.write_all(format!("{},{},{}\n", a, b, n).as_bytes())?;
    }

    Ok(())
}


fn check_n(n: u64) -> impl Iterator<Item=(u64, u64, u64)> {
    let d = next_base_pow(n);
    (0..((BASE - 1) * (BASE - 2)))
        .into_iter()
        .map(|i| (i % (BASE - 1) + 1, i / (BASE - 1) + 2))  // a goes from 1 to BASE - 1, b from 2 to BASE - 1
        .filter_map(move |(a, b)| if check(a, b, n, d) && gcd(a, b) == 1 { Some((a, b, n)) } else { None })
}

fn check(a: u64, b: u64, n: u64, d: u64) -> bool {  // d is the next biggest power of BASE after n
    (a * b) * (d - 1) == (10 * a - b) * n
}

fn next_base_pow(mut n: u64) -> u64 {
    let mut d = 1;
    while n > 0 {
        d *= BASE;
        n /= BASE;
    }
    d
}

fn gcd(mut a: u64, mut b: u64) -> u64 {
    let mut c;
    while b > 0 {
        c = a % b;
        a = b;
        b = c;
    }
    a
}
