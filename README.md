# Fraction Fun

A rust program to find triples of numbers like this:

![7'903'225'806'451'612/9'032'258'064'516'128 = 7 / 8](https://yt3.ggpht.com/COjtafARuvsJwx_Ad0kkqK3Kdo2b9lb-40EkNQz3VXvjTq695IVAcP-PiHjrMOLa9inzGicpxsqhqA=s800-nd)

### Usage

    main.exe [filename [lower-bound [upper-boud]]]
